#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define NUM_THREADS 10


pthread_t threads[NUM_THREADS];
sem_t semaphores[NUM_THREADS];

void *thread_function(void *arg) {
    int thread_num = *((int *)arg);
    sem_wait(&semaphores[thread_num]);
    printf("Thread %d\n", thread_num);
    if (thread_num > 0) {
        sem_post(&semaphores[thread_num - 1]);
    }
    pthread_exit(NULL);
}

int main() {

    for (int i = 0; i < NUM_THREADS; i++) {
        sem_init(&semaphores[i], 0, 0);
    }
    for (int i = 0; i < NUM_THREADS; i++) {
        int *arg = malloc(sizeof(*arg));
        if (arg == NULL) {
            perror("malloc");
            exit(EXIT_FAILURE);
        }
        *arg = i;
        if (pthread_create(&threads[i], NULL, thread_function, arg) != 0) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
    }
    sem_post(&semaphores[NUM_THREADS - 1]);
    for (int i = 0; i < NUM_THREADS; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            perror("pthread_join");
            exit(EXIT_FAILURE);
        }
        sem_destroy(&semaphores[i]);
    }

    return 0;
}

#include <stdio.h>
#include <pthread.h>


void *afficherBonjour(void *arg) {
    int thread_num = *((int *)arg); // Cast de void* à int*
    printf("Thread %d : Bonjour (%d)\n", thread_num, thread_num);
    pthread_exit(NULL);
}

int main() {
    pthread_t threads[3];
    int args[3] = {0, 1, 2};
    int erreur;
    long i;
    for (i = 0; i < 3; i++) {
        erreur = pthread_create(&threads[i], NULL, afficherBonjour, (void *)&args[i]);
        if (erreur) {
            printf("Erreur lors de la création du thread %ld : %d\n", i, erreur);
            return -1;
        }
    }
    for (i = 0; i < 3; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}
